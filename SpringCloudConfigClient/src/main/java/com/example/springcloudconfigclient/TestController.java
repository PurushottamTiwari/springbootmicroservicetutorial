package com.example.springcloudconfigclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {

    Logger logger = LoggerFactory.getLogger(TestController.class);
    @Value("${limits-service.minimum}")
    public String val1;


    @Value("${limits-service.maximum}")
    public String val2;

    @GetMapping(path ="/test")
    public String show(){
        logger.info("Inside Config Client");
       return ("Value1"+ val1+"---"+"Value2"+ val2);

    }
}
