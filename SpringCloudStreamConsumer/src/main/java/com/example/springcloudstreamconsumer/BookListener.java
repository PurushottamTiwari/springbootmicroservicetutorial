package com.example.springcloudstreamconsumer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Component
public class BookListener {



   private Logger logger =  LoggerFactory.getLogger(BookListener.class);

    @StreamListener(Processor.INPUT)
    public void readBook(Book book){
       logger.info("Consumed Message :"+book);

    }
}
