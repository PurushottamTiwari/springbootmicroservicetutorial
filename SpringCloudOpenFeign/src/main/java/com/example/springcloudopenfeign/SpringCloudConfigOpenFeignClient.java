package com.example.springcloudopenfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "A-BOOTIFUL-CLIENT")
public interface SpringCloudConfigOpenFeignClient {

    @GetMapping("/test")
    public String callConfigClient();

}
