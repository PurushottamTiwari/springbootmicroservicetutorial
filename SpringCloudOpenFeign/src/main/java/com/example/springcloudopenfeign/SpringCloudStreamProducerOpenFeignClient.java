package com.example.springcloudopenfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name ="SPRINGCLOUDSTREAMPUBLISHER")
public interface SpringCloudStreamProducerOpenFeignClient {

    @PostMapping("/sendBook")
    public void callProducer(Book book);
}
