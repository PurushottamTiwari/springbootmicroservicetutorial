package com.example.springcloudopenfeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {

    @Autowired
    SpringCloudStreamProducerOpenFeignClient springCloudStreamProducerOpenFeignClient;

    @Autowired
    SpringCloudConfigOpenFeignClient springCloudConfigOpenFeignClient;

    @PostMapping("/callPublisher")
    public String callService(@RequestBody Book book){
        springCloudStreamProducerOpenFeignClient.callProducer(book);
        return "SUCCESS";
    }

    @GetMapping("/callConfig")
    public String callConfigService(){
       return  springCloudConfigOpenFeignClient.callConfigClient();

    }
}
