package com.example.springcloudstreampublisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private Processor pipe;

    @PostMapping(path ="/sendBook")
    @SendTo(Processor.OUTPUT)
    public void sendBook(@RequestBody Book book){
      pipe.output().send(MessageBuilder.withPayload(book).build());
        logger.info("Message Send :"+book);

    }
}
