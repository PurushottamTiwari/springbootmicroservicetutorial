package com.example.microservicetutorial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SpringCloudConfigServer {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(SpringCloudConfigServer.class);
        logger.info("Inside Config Server");
        SpringApplication.run(SpringCloudConfigServer.class, args);
    }

}
