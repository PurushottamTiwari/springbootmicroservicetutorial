package com.example.springcloudhystrix;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController

public class ConfigClientCallController {


    @HystrixCommand(fallbackMethod = "callFallBack")
    @GetMapping("/call")
    public String callConfigClient(){
       return new RestTemplate().getForObject("http://localhost:8765/test",String.class);
    }

    public String callFallBack(){
        return "Service is down.Please try later";
    }
}
