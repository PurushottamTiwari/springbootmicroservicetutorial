package com.example.springcloudnetflixribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudNetflixRibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudNetflixRibbonApplication.class, args);
    }

}
