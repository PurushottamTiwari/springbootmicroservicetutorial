package com.example.springcloudnetflixribbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RibbonClient(name="a-bootiful-client",configuration = RibbonConfigurationClasss.class)
@RestController
public class ConfigRibbonClient {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/call")
    public String call(){
        return restTemplate.getForObject("http://a-bootiful-client/test", String.class);
    }


}
